;;;; puber.lisp

(ql:quickload '(cl-who hunchentoot cl-redis split-sequence))

(redis:connect)

(defparameter *prefix* "puber:")
(defparameter *keys* (concatenate 'string *prefix* "*"))

(defun add-prefix (str)
  (concatenate 'string *prefix* str))

(defun get-host (key &optional (pref t))
  (when pref
    (setq key (add-prefix key)))
  (red:get key))

(defun idx ()
  (setf (hunchentoot:content-type*) "text/html")
  (who:with-html-output-to-string (out)
    (:html
     (:head
      (:title "Puber"))
     (:body
      (:p (who:fmt "I have ~@[ ~D~] key~:P!" (list-length (red:keys *keys*))))))))

(defun list-keys (host-list)
  (setf (hunchentoot:content-type*) "text/plain")
  (let ((resp '()))
    (loop for i in (split-sequence:SPLIT-SEQUENCE #\, host-list) do
	 (let ((host (get-host i t)))
	   (when host
	     (push host resp))))
    (format nil "~{~A~%~}" resp)))

(hunchentoot:define-easy-handler (app :uri "/") (host)
  (if (eq nil host)
      (idx)
      (list-keys host)))

(hunchentoot:define-easy-handler (all :uri "/all") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (let ((resp '()))
    (loop for i in (red:keys *keys*) do
	 (format nil "~@[ ~A~]" (push (get-host i nil) resp)))
    (format nil "~{~A~^~%~}" resp)))

(hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 8080))
