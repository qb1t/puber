# Puber

Puber is a SSH public key store for my personal use. Use at your own risk.


### Getting Key(s) ###
Getting a single key (where the host name is 'abcdef'):
	
	ftp http://server/?host=abcdef

Getting keys for multiple hosts:

	ftp http://server/?host=abcdef,abcdefg,abcdefgh

Getting all available keys:
	
	ftp http://server/all

### Adding Keys ###
Not implemented yet.


